<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('Login');
		$this->load->model('Akses');
		$this->load->model('User');
		$this->load->model('Kategori');
		$this->load->model('Barang');
		$this->load->model('Toko');
		$this->load->model('Supplier');
		$this->load->model('Pembelian');
		$this->load->model('Menu');
		$this->load->model('Penjualan');
	}
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function home()
	{
		$menu['toko'] = $this->Toko->select();
		$menu['menu'] = $this->Menu->selectAll();
		$menu['akses'] = $this->Akses->selectAll();
		//$this->load->view('template/header');
		$data['struk'] = $this->Penjualan->selectstruk();
		$data['barang'] = $this->Barang->cekstok();
		$this->load->view('template/menu', $menu);
		$this->load->view('home', $data);
		$this->load->view('template/bottom');
		//$this->load->view('template/footer');
	}
	public function beranda()
	{
		$data['struk'] = $this->Penjualan->selectstruk();
		$data['barang'] = $this->Barang->cekstok();
		$menu['akses'] = $this->Akses->selectAll();
		//$this->load->view('template/header');
		//$this->load->view('template/menu', $menu);
		$this->load->view('home', $data);
		//$this->load->view('template/footer');
	}
	public function index() {
		if($_POST != NULL) {
			$this->Login->log();
			$q = $this->session->userdata('status');
			if($q == "login") {
				echo "1";
			} else {
				echo "0";
			}
		} else {
			$q = $this->session->userdata('status');
			if($q == "login") {
				$menu['toko'] = $this->Toko->select();
				$menu['menu'] = $this->Menu->selectAll();
				$menu['akses'] = $this->Akses->selectAll();
				$data['struk'] = $this->Penjualan->selectstruk();
				$data['barang'] = $this->Barang->cekstok();
				$this->load->view('template/header');
				$this->load->view('template/menu', $menu);
				$this->load->view('home', $data);
				$this->load->view('template/bottom');
				$this->load->view('template/footer');
			} else {
				$this->load->view('template/header');
				$this->load->view('index');
				$this->load->view('template/footer');
			}
		}
	}
	public function logout() {
		$this->session->sess_destroy();
		//$this->load->view('template/header');
		$this->load->view('index');
		//$this->load->view('template/footer');
	}
	public function user() {
		/*$menu['akses'] = $this->Akses->selectAll();
		$this->load->view('template/header');
		$this->load->view('template/menu', $menu);*/
		$this->load->view('user');
		//$this->load->view('template/footer');
	}
	function jsonuser(){
		header('Content-Type: application/json');
        $data = $this->User->json();
		print_r($data);
    }
	public function modaluser() {
		$data['cek'] = 0;
		$data['akses'] = $this->Akses->selectAll();
		$this->load->view('modal/user', $data);
	}
	public function adduser() {
		$this->User->add();
		echo json_encode(array("status" => TRUE));
	}
	public function edituser($id) {
		$data['cek'] = 1;
		$data['akses'] = $this->Akses->selectAll();
		$data['user'] = $this->User->edit($id);
		$this->load->view('modal/user', $data);
	}
	public function deleteuser($id) {
		$this->User->delete($id);
		echo json_encode(array("status" => TRUE));
	}
	public function updateuser() {
		$this->User->update();
		echo json_encode(array("status" => TRUE));
	}
	public function kategori() {
		/*$menu['akses'] = $this->Akses->selectAll();
		$this->load->view('template/header');
		$this->load->view('template/menu', $menu);*/
		$this->load->view('kategori');
		//$this->load->view('template/footer');
	}
	function jsonkategori(){
		header('Content-Type: application/json');
        $data = $this->Kategori->json();
		print_r($data);
    }
	public function modalkategori() {
		$data['cek'] = 0;
		$this->load->view('modal/kategori', $data);
	}
	public function addkategori() {
		$this->Kategori->add();
		echo json_encode(array("status" => TRUE));
	}
	public function editkategori($id) {
		$data['cek'] = 1;
		$data['kategori'] = $this->Kategori->edit($id);
		$this->load->view('modal/kategori', $data);
	}
	public function deletekategori($id) {
		$this->Kategori->delete($id);
		echo json_encode(array("status" => TRUE));
	}
	public function updatekategori() {
		$this->Kategori->update();
		echo json_encode(array("status" => TRUE));
	}
	public function akses() {
		$this->load->view('akses');
	}
	function jsonakses(){
		header('Content-Type: application/json');
        $data = $this->Akses->json();
		print_r($data);
    }
	public function modalakses() {
		$data['cek'] = 0;
		$data['menu'] = $this->Menu->selectAll();
		$this->load->view('modal/akses', $data);
	}
	public function addakses() {
		$this->Akses->add();
		echo json_encode(array("status" => TRUE));
	}
	public function editakses($id) {
		$data['cek'] = 1;
		$data['menu'] = $this->Menu->selectAll();
		$data['akses'] = $this->Akses->edit($id);
		$this->load->view('modal/akses', $data);
	}
	public function deleteakses($id) {
		$this->Akses->delete($id);
		echo json_encode(array("status" => TRUE));
	}
	public function updateakses() {
		$this->Akses->update();
		echo json_encode(array("status" => TRUE));
	}
	public function barang() {
		$data['barang'] = $this->Barang->selectAll();
		$this->load->view('barang', $data);
	}
	function jsonbarang(){
		header('Content-Type: application/json');
        $data = $this->Barang->json();
		print_r($data);
    }
	public function modalbarang() {
		$data['cek'] = 0;
		$data['kategori'] = $this->Kategori->selectAll();
		$this->load->view('modal/barang', $data);
	}
	public function addbarang() {
		$this->Barang->add();
		echo json_encode(array("status" => TRUE));
	}
	public function editbarang($id) {
		$data['cek'] = 1;
		$data['kategori'] = $this->Kategori->selectAll();
		$data['barang'] = $this->Barang->edit($id);
		$this->load->view('modal/barang', $data);
	}
	public function deletebarang($id) {
		$this->Barang->delete($id);
		echo json_encode(array("status" => TRUE));
	}
	public function updatebarang() {
		$this->Barang->update();
		echo json_encode(array("status" => TRUE));
	}
	public function toko() {
		$this->load->view('toko');
	}
	function jsontoko(){
		header('Content-Type: application/json');
        $data = $this->Toko->json();
		print_r($data);
    }
	public function edittoko($id) {
		$data['cek'] = 1;
		$data['toko'] = $this->Toko->edit($id);
		$this->load->view('modal/toko', $data);
	}
	public function updatetoko() {
		$this->Toko->update();
		echo json_encode(array("status" => TRUE));
	}
	public function supplier() {
		$this->load->view('supplier');
	}
	function jsonsupplier(){
		header('Content-Type: application/json');
        $data = $this->Supplier->json();
		print_r($data);
    }
	public function modalsupplier() {
		$data['cek'] = 0;
		$this->load->view('modal/supplier', $data);
	}
	public function addsupplier() {
		$this->Supplier->add();
		echo json_encode(array("status" => TRUE));
	}
	public function editsupplier($id) {
		$data['cek'] = 1;
		$data['supplier'] = $this->Supplier->edit($id);
		$this->load->view('modal/supplier', $data);
	}
	public function deletesupplier($id) {
		$this->Supplier->delete($id);
		echo json_encode(array("status" => TRUE));
	}
	public function updatesupplier() {
		$this->Supplier->update();
		echo json_encode(array("status" => TRUE));
	}
	public function pembelian() {
		$data['pembelian'] = $this->Pembelian->selectAll();
		$this->load->view('pembelian', $data);
	}
	public function modalpembelian() {
		$data['cek'] = 0;
		$data['supplier'] = $this->Supplier->selectAll();
		$data['barang'] = $this->Barang->selectAll();
		$this->load->view('modal/pembelian', $data);
	}
	public function addpembelian() {
		$this->Pembelian->add();
		echo json_encode(array("status" => TRUE));
	}
	public function editpembelian($id) {
		$data['cek'] = 1;
		$data['supplier'] = $this->Supplier->selectAll();
		$data['barang'] = $this->Barang->selectAll();
		$data['pembelian'] = $this->Pembelian->edit($id);
		$this->load->view('modal/pembelian', $data);
	}
	public function deletepembelian($id) {
		$this->Pembelian->delete($id);
		echo json_encode(array("status" => TRUE));
	}
	public function updatepembelian() {
		$this->Pembelian->update();
		echo json_encode(array("status" => TRUE));
	}
	public function kasir() {
		$barang = $this->Barang->selectAll();
		$arr = array();
		foreach ($barang as $key) {
				$arr[] = array('value' => $key->id_barang, 'label' => $key->nama_barang);
		}
		$data['arr'] = json_encode($arr);
		$data['barang'] = $barang;
		$data['sementara'] = $this->Barang->sementara();
		$this->load->view('kasir', $data);
	}
	public function caribarang() {
		$data['barang'] = $this->Barang->selectAll();
		$data['sementara'] = $this->Barang->cari();
		$this->load->view('modal/transaksi', $data);
	}
	public function deletesementara($id) {
		$this->Barang->deletesementara($id);
		$data['barang'] = $this->Barang->selectAll();
		$data['sementara'] = $this->Barang->sementara();
		$this->load->view('modal/transaksi', $data);
	}
	public function cetak() {
		$c = $this->Penjualan->transaksi();
		$data['toko'] = $this->Toko->select();
		$data['bayar'] = $this->input->post('bayar');
		$data['kembali'] = $this->input->post('kembalian');
		$data['barang'] = $this->Barang->selectAll();
		$data['penjualan'] = $this->Penjualan->cek($c);
		$data['struk'] = $this->Penjualan->struk($c);
		$this->load->view('cetak', $data);
	}
	public function cetak1($id) {
		$data['bayar'] = "0";
		$data['kembali'] = "0";
		$data['barang'] = $this->Barang->selectAll();
		$data['toko'] = $this->Toko->select();
		$data['penjualan'] = $this->Penjualan->cek($id);
		$data['struk'] = $this->Penjualan->struk($id);
		$this->load->view('cetak', $data);
	}
	public function menu() {
		$this->load->view('menu');
	}
	function jsonmenu(){
		header('Content-Type: application/json');
        $data = $this->Menu->json();
		print_r($data);
    }
	public function modalmenu() {
		$data['cek'] = 0;
		$this->load->view('modal/menu', $data);
	}
	public function addmenu() {
		$this->Menu->add();
		echo json_encode(array("status" => TRUE));
	}
	public function editmenu($id) {
		$data['cek'] = 1;
		$data['menu'] = $this->Menu->edit($id);
		$this->load->view('modal/menu', $data);
	}
	public function deletemenu($id) {
		$this->Menu->delete($id);
		echo json_encode(array("status" => TRUE));
	}
	public function updatemenu() {
		$this->Menu->update();
		echo json_encode(array("status" => TRUE));
	}
	function jsonpenjualan(){
		header('Content-Type: application/json');
        $data = $this->Penjualan->json();
		print_r($data);
    }
	public function penjualan() {
		$this->load->view('penjualan');
	}
}
