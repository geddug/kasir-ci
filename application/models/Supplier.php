<?php
class Supplier extends CI_model {
	public function selectAll() {
		$status = "1";
		$this->db->where('status', $status);
		$this->db->order_by('id_supplier', "asc");
    return $this->db->get('supplier')->result();
  }
  public function json() {
	  $status = 1;
		$this->datatables->select('id_supplier,nama,alamat,telp,status');
		$this->datatables->where('status', $status);
        $this->datatables->from('supplier');
		$this->datatables->add_column('action', '<a href="#" onclick="ganti($1)" class="table-action-btn h3"><i class="mdi mdi-pencil-box-outline text-success"></i></a> 
		<a href="#" onclick="hapus($1)" class="table-action-btn h3"><i class="mdi mdi-close-box-outline text-danger"></i></a>', 'id_supplier');
        return $this->datatables->generate();
  }
  public function add() {
		$status = "1";
		$nama = $this->input->post('nama');
		$alamat = $this->input->post('alamat');
		$telp = $this->input->post('telp');
		$data = array('nama' => $nama, 'alamat' => $alamat, 'telp' => $telp, 'status' => $status);
		$this->db->insert('supplier', $data);
		$this->db->insert_id();
	}
	public function delete($id){
		$this->db->where('id_supplier', $id);
		$this->db->update('supplier', array('status' => "0"));
	}
	public function edit($id){
		$this->db->where('id_supplier', $id);
		return $this->db->get('supplier')->row();
	}
	public function update(){
		$id = $this->input->post('id_supplier');
		$nama = $this->input->post('nama');
		$alamat = $this->input->post('alamat');
		$telp = $this->input->post('telp');
		$data = array('nama' => $nama, 'alamat' => $alamat, 'telp' => $telp);
		$this->db->where('id_supplier', $id);
		$this->db->update('supplier', $data);
	}
}