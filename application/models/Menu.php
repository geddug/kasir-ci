<?php
class Menu extends CI_model {
	public function selectAll() {
		$status = "1";
		$this->db->where('status', $status);
		$this->db->order_by('menu', "asc");
    return $this->db->get('menu')->result();
  }
  public function json() {
	  $status = 1;
		$this->datatables->select('id_menu,menu,status');
		$this->datatables->where('status', $status);
        $this->datatables->from('menu');
		$this->datatables->add_column('action', '<a href="#" onclick="ganti($1)" class="table-action-btn h3"><i class="mdi mdi-pencil-box-outline text-success"></i></a> 
		<a href="#" onclick="hapus($1)" class="table-action-btn h3"><i class="mdi mdi-close-box-outline text-danger"></i></a>', 'id_menu');
        return $this->datatables->generate();
  }
  public function add() {
		$status = "1";
		$menu = $this->input->post('menu');
		$data = array('menu' => $menu, 'status' => $status);
		$this->db->insert('menu', $data);
		$this->db->insert_id();
	}
	public function delete($id){
		$this->db->where('id_menu', $id);
		$this->db->update('menu', array('status' => "0"));
		
	}
	public function edit($id){
		$this->db->where('id_menu', $id);
		return $this->db->get('menu')->row();
	}
	public function update(){
		$id = $this->input->post('id_menu');
		$menu = $this->input->post('menu');
		$data = array('menu' => $menu);
		$this->db->where('id_menu', $id);
		$this->db->update('menu', $data);
		
	}
}