<?php
class Barang extends CI_model {
	public function selectAll() {
		$status = "1";
		$this->db->select("*");
		$this->db->from("barang");
		$this->db->join('kategori', 'barang.kategori_id = kategori.id_kategori');
		$this->db->where('barang.status', $status);
		$this->db->order_by('barang.id_barang', "asc");
    return $this->db->get()->result();
  }
  public function cekstok() {
	  $this->db->where('status', 1);
	  $this->db->where('stok <='. 3);
	  return $this->db->get('barang')->result();
  }
  public function json() {
	  $status = 1;
		$this->datatables->select('id_barang,nama_barang,jual,stok');
		$this->datatables->join('kategori', 'barang.kategori_id = kategori.id_kategori');
		$this->datatables->where('barang.status', $status);
		$this->datatables->select('kategori.nama');
        $this->datatables->from('barang');
		$this->datatables->add_column('action', '<a href="#" onclick="ganti($1)" class="table-action-btn h3"><i class="mdi mdi-pencil-box-outline text-success"></i></a> 
		<a href="#" onclick="hapus($1)" class="table-action-btn h3"><i class="mdi mdi-close-box-outline text-danger"></i></a>', 'id_barang');
        return $this->datatables->generate();
  }
  public function add() {
		$status = "1";
		$nama_barang = $this->input->post('nama_barang');
		$kategori_id = $this->input->post('kategori_id');
		$beli = 0;
		$jual = $this->input->post('jual');
		$stok = 0;
		$untung = 0;
		$data = array('nama_barang' => $nama_barang, 'kategori_id' => $kategori_id, 'beli' => $beli, 'jual' => $jual,
					'stok' => $stok, 'untung' => $untung, 'status' => $status);
		$this->db->insert('barang', $data);
		$this->db->insert_id();
	}
	public function delete($id) {
		$this->db->where('id_barang', $id);
		$this->db->update('barang', array('status' => "0"));
	}
	public function edit($id) {
		$this->db->where('id_barang', $id);
		return $this->db->get('barang')->row();
	}
	public function update() {
		$id = $this->input->post('id_barang');
		$nama_barang = $this->input->post('nama_barang');
		$kategori_id = $this->input->post('kategori_id');
		$jual = $this->input->post('jual');
		$beli = $this->input->post('beli');
		
		$data = array('nama_barang' => $nama_barang, 'kategori_id' => $kategori_id, 'jual' => $jual, 'untung' => $jual-$beli);
		$this->db->where('id_barang', $id);
		$this->db->update('barang', $data);
	}
	public function cari() {
		$id = $this->input->post('barang');
		$this->db->where('id_barang', $id);
		$c = $this->db->get('barang')->row();
		if($c != NULL) {
			if($c->stok > 0) {
				$this->db->where('barang_id', $id);
				$sementara = $this->db->get('sementara')->row();
				if($sementara != NULL) {
					if($sementara->jumlah+1 <= $c->stok) {
						$this->db->where('id_sementara', $sementara->id_sementara);
						$this->db->update('sementara', array('jumlah' => $sementara->jumlah+1));
					} 
				} else {
					$data = array('barang_id' => $id, 'jumlah' => "1", 'harga' => $c->jual);
					$this->db->insert('sementara', $data);
					$this->db->insert_id();
				}
			}
		}
		return $this->db->get('sementara')->result();
	}
	public function sementara() {
		return $this->db->get('sementara')->result();
	}
	public function deletesementara($id) {
		$this->db->delete('sementara', array('id_sementara' => $id)); 
	}
}