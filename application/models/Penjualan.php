<?php
class Penjualan extends CI_model {
	public function selectstruk() {
		$this->db->order_by('id_struk', "desc");
		return $this->db->get('struk')->result();
	}
	public function transaksi() {
		date_default_timezone_set('Asia/Jakarta');
        $kodedate = date('jmy');
        $date1    = date('Y-m-d');
        $waktu    = date('Y-m-d G:i:s');
        
        $this->db->select_max('id_struk');
        $cek = $this->db->get('struk')->row();
        if ($cek != NULL) {
            $kode  = $cek->id_struk;
            $kode1 = substr($kode, -3);
            $kode2 = $kode1 + 1;
        } else {
            $kode2 = 001;
        }
		$kodeunik = $kodedate . str_pad($kode2, 3, "0", STR_PAD_LEFT);
		$total = $this->input->post('total');
		$data = array('id_struk' => $kodeunik, 'total_harga' => $total, 'tgl' => $waktu);
		$this->db->insert('struk', $data);
		$s = $this->db->get('sementara')->result();
		foreach ($s as $key) {
			$data1 = array('barang_id' => $key->barang_id, 'qty' => $key->jumlah, 'struk_id' => $kodeunik);
			$this->db->insert('penjualan', $data1);
			
			$this->db->where('id_barang', $key->barang_id);
			$b = $this->db->get('barang')->row();
			$this->db->where('id_barang', $key->barang_id);
			$this->db->update('barang', array('stok' => $b->stok - $key->jumlah));
		}
		foreach ($s as $key) {
			$this->db->delete('sementara', array('id_sementara' => $key->id_sementara)); 
		}
		return $kodeunik;
	}
	public function cek($id) {
		$this->db->where('struk_id', $id);
		return $this->db->get('penjualan')->result();
	}
	public function struk($id) {
		$this->db->where('id_struk', $id);
		return $this->db->get('struk')->row();
	}
	public function json()
      {
        $status = 1;
        $this->datatables->select('id_struk,total_harga,tgl');
        $this->datatables->from('struk');
        $this->datatables->add_column('action', '<a href="#" onclick="cetak($1)" class="table-action-btn h3"><i class="mdi mdi-printer text-success"></i></a>', 'id_struk');
        return $this->datatables->generate();
      }
}