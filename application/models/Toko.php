<?php
class Toko extends CI_model {
	public function select() {
		$status = "1";
    return $this->db->get('toko')->row();
  }
  public function json() {
	  $status = 1;
		$this->datatables->select('id_toko,nama_toko,alamat,telp');
        $this->datatables->from('toko');
		$this->datatables->add_column('action', '<a href="#" onclick="ganti($1)" class="table-action-btn h3"><i class="mdi mdi-pencil-box-outline text-success"></i></a>', 'id_toko');
        return $this->datatables->generate();
  }
	public function edit($id){
		$this->db->where('id_toko', $id);
		return $this->db->get('toko')->row();
	}
	public function update(){
		$id = $this->input->post('id_toko');
		$nama_toko = $this->input->post('nama_toko');
		$alamat = $this->input->post('alamat');
		$telp = $this->input->post('telp');
		$catatan = nl2br($this->input->post('catatan'));
		$data = array('nama_toko' => $nama_toko, 'alamat' => $alamat, 'telp' => $telp, 'catatan' => $catatan);
		$this->db->where('id_toko', $id);
		$this->db->update('toko', $data);
	}
}