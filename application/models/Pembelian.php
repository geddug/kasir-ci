<?php
class Pembelian extends CI_model {
	public function selectAll() {
		$this->db->select("*");
		$this->db->from("pembelian");
		$this->db->join('barang', 'pembelian.barang_id = barang.id_barang');
		$this->db->join('supplier', 'pembelian.supplier_id = supplier.id_supplier');
		$this->db->join('user', 'pembelian.user_id = user.id_user');
		$this->db->order_by('pembelian.id_pembelian', "desc");
    return $this->db->get()->result();
  }
  public function add() {
		date_default_timezone_set('Asia/Jakarta');
		$date = date('Y-m-d H:i:s');
		$user_id = $this->session->userdata('user_id');
		$barang_id = $this->input->post('barang_id');
		$qty = $this->input->post('qty');
		$total = $this->input->post('total');
		$supplier_id = $this->input->post('supplier_id');
		$data = array('barang_id' => $barang_id, 'qty' => $qty, 'total' => $total,
					'supplier_id' => $supplier_id, 'user_id' => $user_id, 'tgl' => $date);
		$this->db->insert('pembelian', $data);
		$this->db->insert_id();
		
		$this->db->where('id_barang', $barang_id);
		$b = $this->db->get('barang')->row();
		
		$databarang = array('stok' => $b->stok+$qty, 'beli' => $total/$qty, 'untung' => $b->jual - $total/$qty);
		$this->db->update('barang', $databarang);
	}
	public function edit($id) {
		$this->db->where('id_pembelian', $id);
		return $this->db->get('pembelian')->row();
	}
	public function update() {
		$id = $this->input->post('id_pembelian');
		$user_id = $this->session->userdata('user_id');
		$barang_id = $this->input->post('barang_id');
		$qty = $this->input->post('qty');
		$total = $this->input->post('total');
		$supplier_id = $this->input->post('supplier_id');
		
		$this->db->where('id_pembelian', $id);
		$p = $this->db->get('pembelian')->row();
		
		$this->db->where('id_barang', $barang_id);
		$b = $this->db->get('barang')->row();
		
		$databarang1 = array('stok' => $b->stok - $p->qty + $qty, 'beli' => $total/$qty, 'untung' => $b->jual - $total/$qty);
		$this->db->update('barang', $databarang1);
		
		$data = array('barang_id' => $barang_id, 'qty' => $qty, 'total' => $total,
					'supplier_id' => $supplier_id, 'user_id' => $user_id);
		$this->db->where('id_pembelian', $id);
		$this->db->update('pembelian', $data);
		
	}
}