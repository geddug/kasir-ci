<?php
   if($this->session->userdata('status') != "login"){
   			redirect(base_url());
   		}else {
   		}
   ?>
   <!-- Sweet Alert -->
      <link href="<?php echo base_url();?>assets/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css">
	   <!-- Sweet-Alert  -->
      <script src="<?php echo base_url();?>assets/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
<!-- Loader -->
<!--<div id="preloader">
   <div id="status">
       <div class="spinner">
         <div class="spinner-wrapper">
           <div class="rotator">
             <div class="inner-spin"></div>
             <div class="inner-spin"></div>
           </div>
         </div>
       </div>
   </div>
   </div>-->
<!-- Begin page -->
<div id="wrapper">
<!-- Top Bar Start -->
<div class="topbar">
   <!-- LOGO -->
   <div class="topbar-left">
      <a href="<?php echo base_url();?>" class="logo"><?php echo $toko->nama_toko;?><i class="mdi mdi-cube"></i></a>
      <!-- Image logo -->
      <!--<a href="<?php echo base_url();?>" class="logo">-->
      <!--<span>-->
      <!--<img src="assets/images/logo.png" alt="" height="30">-->
      <!--</span>-->
      <!--<i>-->
      <!--<img src="assets/images/logo_sm.png" alt="" height="28">-->
      <!--</i>-->
      <!--</a>-->
   </div>
   <!-- Button mobile view to collapse sidebar menu -->
   <div class="navbar navbar-default" role="navigation">
      <div class="container">
         <!-- Navbar-left -->
         <ul class="nav navbar-nav navbar-left">
            <li>
               <button id="navigasi" class="button-menu-mobile open-left waves-effect waves-light">
               <i class="mdi mdi-menu"></i>
               </button>
            </li>

         </ul>

      </div>
      <!-- end container -->
   </div>
   <!-- end navbar -->
</div>
<!-- Top Bar End -->
<!-- ========== Left Sidebar Start ========== -->
<div class="left side-menu">
   <div class="sidebar-inner slimscrollleft">
      <!--- Sidemenu -->
      <div id="sidebar-menu">
         <div class="user-details">
            <div class="overlay"></div>
            <div class="text-center">
               <img src="<?php echo base_url();?>assets/images/user.png" alt="" class="thumb-md img-circle">
            </div>
            <div class="user-info">
               <div>
                  <a href="#setting-dropdown" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> 
                  <?php echo $this->session->userdata('nama_lengkap');?> <span class="mdi mdi-menu-down"></span>
                  </a>
               </div>
            </div>
         </div>
         <div class="dropdown" id="setting-dropdown">
            <ul class="dropdown-menu">
               <li><a href="javascript:void(0)"><i class="mdi mdi-face-profile m-r-5"></i> Profile</a></li>
               <li><a href="javascript:void(0)"><i class="mdi mdi-account-settings-variant m-r-5"></i> Settings</a></li>
               <li><a href="javascript:void(0)"><i class="mdi mdi-lock m-r-5"></i> Lock screen</a></li>
               <li><a style="cursor: pointer;" onclick="isibody('logout');"><i class="mdi mdi-logout m-r-5"></i> Logout</a></li>
            </ul>
         </div>
         <ul>
            <li class="menu-title">Navigation</li>
            <li class="has_sub">
               <a style="cursor: pointer;" onclick="pindah('beranda');" class="waves-effect"><i class="mdi mdi-view-dashboard"></i><span class="label label-success pull-right"></span> <span> Beranda </span> </a>
            </li>
            <?php foreach ($menu as $keym) {?>
            <li class="has_sub">
               <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-layers"></i><span> <?php echo $keym->menu;?> </span> <span class="menu-arrow"></span></a>
               <ul class="list-unstyled">
                  <?php $akses_id = $this->session->userdata('akses_id');
                     $res1 = explode(',',$akses_id);
                     foreach ($akses as $key) {
                     	foreach ($res1 as $keya => $value) {
                     		if ($value == $key->id_akses) { 
                     if($key->menu_id == $keym->id_menu) { ?>
                  <li class="has_sub">
                     <a style="cursor: pointer;" onclick="pindah('<?php echo strtolower($key->akses);?>');" class="waves-effect"><span> <?php echo ucfirst($key->akses);?> </span></a>
                  </li>
                  <?php } } } } ?>
               </ul>
            </li>
            <?php } ?>
         </ul>
      </div>
      <!-- Sidebar -->
      <div class="clearfix"></div>
   </div>
   <!-- Sidebar -left -->
</div>
<!-- Left Sidebar End -->
<div id="pindah">
