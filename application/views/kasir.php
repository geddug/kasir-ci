<script>
$(function () {
	var projects = <?php echo $arr;?>;

	$("#barang").autocomplete({
			minLength: 0,
			source: projects,
			focus: function (event, ui) {
				$("#barang").val(ui.item.label);
				return false;
			},
			select: function (event, ui) {
				$("#barang").val(ui.item.value);
				$.ajax({
					url: "<?php echo base_url();?>caribarang",
					type: "POST",
					data: $("#caribarang").serialize(),
					success: function (data) {
						$("#trans").html(data);
						$('#caribarang')[0].reset();
					},
					error: function (jqXHR, textStatus, errorThrown) {
						swal("Error", "", "error");
					}
				});
				return false;
			}
		})
		.autocomplete("instance")._renderItem = function (ul, item) {
			return $("<li>")
				.append("<div>" + item.label + "</div>")
				.appendTo(ul);
		};
});
</script>
<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
   <!-- Start content -->
   <div class="content">
      <div class="container">
         <div class="row">
            <div class="col-xs-12">
               <div class="page-title-box">
                  <h4 class="page-title">Kasir </h4>
                  <ol class="breadcrumb p-0 m-0">
                     <li>
                        <a href="#">Kasir</a>
                     </li>
                     <li class="active">
                        Kasir
                     </li>
                  </ol>
                  <div class="clearfix"></div>
               </div>
            </div>
         </div>
         <!-- end row -->
         <div class="row">
            <div class="col-sm-12">
               <div class="card-box table-responsive">
                  <h4 class="m-t-0 header-title">
                     <b>
                        <form id="caribarang">
                           <div class="pull-right">
                              <input type="text" id="barang" name="barang" class="form-control" placeholder="Cari"><br>
                           </div>
                        </form>
                     </b>
                  </h4>
                  <div id="trans">
                     <table class="table m-0 table-colored table-info">
                        <thead>
                           <tr>
                              <th>#</th>
                              <th>Nama Barang</th>
                              <th>Jumlah</th>
                              <th>Total harga</th>
                              <th width="5%"></th>
                           </tr>
                        </thead>
                        <tbody>
                           <?php $no=1; $total = array(); foreach($sementara as $keys) { 
                              foreach($barang as $key) { 
                              if($key->id_barang == $keys->barang_id) { ?>
                           <tr>
                              <td><?php echo $no;?></td>
                              <td><?php echo $key->nama_barang;?></td>
                              <td><?php echo $keys->jumlah;?></td>
                              <td><?php echo number_format($key->jual * $keys->jumlah);?></td>
                              <td><a href="#" onclick="hapus('<?php echo $keys->id_sementara;?>')" class="table-action-btn h3"><i class="mdi mdi-close-box-outline text-danger"></i></a></td>
                           </tr>
                           <?php $total[]= $key->jual * $keys->jumlah; $no++; } } } ?>
                        </tbody>
                     </table>
                     <table class="table">
                        <tr>
                           <td width="20%"></td>
                           <td width="20%"></td>
                           <td width="25%"></td>
                           <td>
                              <h3>Rp. <?php echo number_format(array_sum($total));?></h3>
                           </td>
                        </tr>
                     </table>
                     <form class="form-inline" id="cetak">
                        <input type="text" id="bayar" onkeyup="myFunction()" name="bayar" class="form-control" placeholder="Bayar" required>
                        <input type="hidden" value="<?php echo array_sum($total);?>" name="total" id="total">
                        <input type="text" id="kembalian" name="kembalian" class="form-control" readonly="true" placeholder="Kembalian" required>
                        <button type="submit" class="btn btn-primary">Cetak</button>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- container -->
   </div>
   <!-- content -->
   <footer class="footer text-right">
      <?php echo date("Y");?> © Yaha. 
      <button class="btn btn-facebook waves-effect waves-light" type="button"> 
      <i class="fa fa-facebook"></i>
      </button>
      <button class="btn btn-instagram waves-effect waves-light" type="button">
      <i class="fa fa-instagram"></i>
      </button>
      <button class="btn btn-github waves-effect waves-light" type="button">
      <i class="fa fa-github"></i>
      </button>
   </footer>
</div>
<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ============================================================== -->
<!-- sample modal content -->
<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="myModalLabel">Form</h4>
         </div>
         <form class="form-horizontal" role="form" id="form">
            <div class="modal-body" id="modalbody">
            </div>
            <div class="modal-footer" id="loading">
               <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
               <button type="submit" class="btn btn-primary waves-effect waves-light">Simpan</button>
            </div>
         </form>
      </div>
      <!-- /.modal-content -->
   </div>
   <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<script>
function myFunction(){var a=$("#bayar").val()-$("#total").val();$("#kembalian").val(a)};
</script>
<script>
$("#caribarang").on("submit",(function(c){c.preventDefault();var d;$.ajax({url:"<?php echo base_url();?>caribarang",type:"POST",data:new FormData(this),contentType:false,cache:false,processData:false,success:function(a){$("#trans").html(a);$("#caribarang")[0].reset()},error:function(f,a,b){swal("Error","","error")}});return false}));$("#cetak").on("submit",(function(b){b.preventDefault();var a=$("#total").val();if(a>0){$.ajax({url:"<?php echo base_url();?>cetak",type:"POST",data:new FormData(this),contentType:false,cache:false,processData:false,success:function(c){var d=window.open("","","width=300,height=300");d.document.write(c);d.document.close();d.focus();d.print();d.close();$.ajax({url:"<?php echo base_url();?>caribarang",type:"POST",data:$("#caribarang").serialize(),success:function(e){$("#trans").html(e);$("#caribarang")[0].reset()},error:function(e,g,f){swal("Error","","error")}})},error:function(e,d,c){swal("Error","","error")}});return false}}));function hapus(b){$.ajax({url:"<?php echo base_url()?>deletesementara/"+b,type:"POST",success:function(a){$("#trans").html(a)},error:function(a,e,f){swal("Error","","error")}})};
</script>
