<?php date_default_timezone_set('Asia/Jakarta');?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title>Print</title>
      <style type="text/css">
         body {
         font-family: Arial, Helvetica, sans-serif;
         font-size: 8pt;
         color: #000;
         text-transform: uppercase;}
      </style>
   </head>
   <body onload="window.print()">
      <table width="250" border="0" align="center">
         <tr>
            <td colspan="4" align="left"><?php echo $this->session->userdata('nama_lengkap');?> &nbsp;</td>
         </tr>
         <tr>
            <td colspan="4">RECEIPT <?php echo $struk->id_struk; ?></td>
         </tr>
         <tr>
            <td height="17">BRG</td>
            <td>QTY</td>
            <td>HRG</td>
            <td>SUB TTL</td>
         </tr>
         <?php $no=1; $total = array(); foreach($penjualan as $keyp) { 
            foreach($barang as $key) { 
            if($key->id_barang == $keyp->barang_id) { ?>
         <tr>
            <td><?php echo $key->nama_barang;?></td>
            <td><?php echo $keyp->qty;?></td>
            <td><?php echo number_format($key->jual * $keyp->qty);?></td>
            <td><?php echo number_format($key->jual);?></td>
         </tr>
         <?php $total[]= $key->jual * $keyp->qty; $no++; } } } ?>
         <tr>
            <td>TOTAL</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td> <?php echo "Rp. " .number_format(array_sum($total)); ?></td>
         </tr>
         <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>CASH</td>
            <td> <?php echo "Rp. " .number_format($bayar); ?></td>
         </tr>
         <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>KEMBALI</td>
            <td> <?php echo "Rp. " .number_format($kembali); ?></td>
         </tr>
         <tr>
            <td colspan="4" align="center">
               <p>&nbsp;</p>
               <p>TERIMA KASIH ATAS KUNJUNGAN ANDA</p>
               <p><?php echo $toko->nama_toko;?></p>
            </td>
         </tr>
      </table>
   </body>
</html>
