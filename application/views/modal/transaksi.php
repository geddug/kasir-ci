<table class="table m-0 table-colored table-info">
   <thead>
      <tr>
         <th>#</th>
         <th>Nama Barang</th>
         <th>Jumlah</th>
         <th>Total harga</th>
         <th width="5%"></th>
      </tr>
   </thead>
   <tbody>
      <?php $no=1; $total = array(); foreach($sementara as $keys) { 
         foreach($barang as $key) { 
         if($key->id_barang == $keys->barang_id) { ?>
      <tr>
         <td><?php echo $no;?></td>
         <td><?php echo $key->nama_barang;?></td>
         <td><?php echo $keys->jumlah;?></td>
         <td><?php echo number_format($key->jual * $keys->jumlah);?></td>
         <td><a href="#" onclick="hapus('<?php echo $keys->id_sementara;?>')" class="table-action-btn h3"><i class="mdi mdi-close-box-outline text-danger"></i></a></td>
      </tr>
      <?php $total[]= $key->jual * $keys->jumlah; $no++; } } } ?>
   </tbody>
</table>
<table class="table">
   <tr>
      <td width="20%"></td>
      <td width="20%"></td>
      <td width="25%"></td>
      <td>
         <h3>Rp. <?php echo number_format(array_sum($total));?></h3>
      </td>
   </tr>
</table>
<form class="form-inline" id="cetak">
   <input type="text" id="bayar" onkeyup="myFunction()" name="bayar" class="form-control" placeholder="Bayar" required>
   <input type="hidden" value="<?php echo array_sum($total);?>" name="total" id="total">
   <input type="text" id="kembalian" name="kembalian" class="form-control" readonly="true" placeholder="Kembalian" required>
   <button type="submit" class="btn btn-primary">Cetak</button>
</form>
<script>
$("#cetak").on("submit",(function(c){c.preventDefault();var d=$("#total").val();if(d>0){$.ajax({url:"<?php echo base_url();?>cetak",type:"POST",data:new FormData(this),contentType:false,cache:false,processData:false,success:function(b){var a=window.open("","","width=300,height=300");a.document.write(b);a.document.close();a.focus();a.print();a.close();$.ajax({url:"<?php echo base_url();?>caribarang",type:"POST",data:$("#caribarang").serialize(),success:function(f){$("#trans").html(f);$("#caribarang")[0].reset()},error:function(j,h,i){swal("Error","","error")}})},error:function(f,a,b){swal("Error","","error")}});return false}}));
</script>
