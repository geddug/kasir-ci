<?php if($cek == 0) { ?>
<div class="form-group">
   <label class="col-md-2 control-label">Barang</label>
   <div class="col-md-10">
      <select name="barang_id" class="form-control select2" required>
         <option style="display:none">Select</option>
         <?php foreach($barang as $key) { ?>
         <option value="<?php echo $key->id_barang;?>"><?php echo $key->nama_barang;?></option>
         <?php } ?>
      </select>
   </div>
</div>
<div class="form-group">
   <label class="col-md-2 control-label">Qty</label>
   <div class="col-md-10">
      <input class="form-control" name="qty" type="text" placeholder="Jumlah" required>
   </div>
</div>
<div class="form-group">
   <label class="col-md-2 control-label">Total Harga</label>
   <div class="col-md-10">
      <input class="form-control" name="total" type="text" placeholder="Jumlah" required>
   </div>
</div>
<div class="form-group">
   <label class="col-md-2 control-label">Supplier</label>
   <div class="col-md-10">
      <select name="supplier_id" class="form-control select2" required>
         <option style="display:none">Select</option>
         <?php foreach($supplier as $key) { ?>
         <option value="<?php echo $key->id_supplier;?>"><?php echo $key->nama;?></option>
         <?php } ?>
      </select>
   </div>
</div>
<?php } else { ?>
<input type="hidden" name="id_pembelian" value="<?php echo $pembelian->id_pembelian;?>">
<div class="form-group">
   <label class="col-md-2 control-label">Barang</label>
   <div class="col-md-10">
      <select name="barang_id" class="form-control select2" required>
         <option style="display:none">Select</option>
         <?php foreach($barang as $key) { ?>
         <option <?php if($pembelian->barang_id == $key->id_barang) { echo "selected"; } ?> value="<?php echo $key->id_barang;?>"><?php echo $key->nama_barang;?></option>
         <?php } ?>
      </select>
   </div>
</div>
<div class="form-group">
   <label class="col-md-2 control-label">Qty</label>
   <div class="col-md-10">
      <input class="form-control" value="<?php echo $pembelian->qty;?>" name="qty" type="text" placeholder="Jumlah" required>
   </div>
</div>
<div class="form-group">
   <label class="col-md-2 control-label">Total Harga</label>
   <div class="col-md-10">
      <input class="form-control" value="<?php echo $pembelian->total;?>" name="total" type="text" placeholder="Jumlah" required>
   </div>
</div>
<div class="form-group">
   <label class="col-md-2 control-label">Supplier</label>
   <div class="col-md-10">
      <select name="supplier_id" class="form-control select2" required>
         <option style="display:none">Select</option>
         <?php foreach($supplier as $key) { ?>
         <option <?php if($pembelian->supplier_id == $key->id_supplier) { echo "selected"; } ?> value="<?php echo $key->id_supplier;?>"><?php echo $key->nama;?></option>
         <?php } ?>
      </select>
   </div>
</div>
<?php } ?>
