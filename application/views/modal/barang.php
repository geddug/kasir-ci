<?php if($cek == 0) { ?>
<div class="form-group">
   <label class="col-md-2 control-label">Nama Barang</label>
   <div class="col-md-10">
      <input class="form-control" name="nama_barang" type="text" placeholder="Nama Barang" required>
   </div>
</div>
<div class="form-group">
   <label class="col-md-2 control-label">Kategori</label>
   <div class="col-md-10">
      <select name="kategori_id" class="form-control" required>
         <option style="display:none">Select</option>
         <?php foreach($kategori as $key) { ?>
         <option value="<?php echo $key->id_kategori;?>"><?php echo $key->nama;?></option>
         <?php } ?>
      </select>
   </div>
</div>
<div class="form-group">
   <label class="col-md-2 control-label">Harga Jual</label>
   <div class="col-md-10">
      <input class="form-control" name="jual" type="text" placeholder="Harga Jual" required>
   </div>
</div>
<?php } else { ?>
<input type="hidden" name="beli" value="<?php echo $barang->beli;?>" >
<input type="hidden" name="id_barang" value="<?php echo $barang->id_barang;?>">
<div class="form-group">
   <label class="col-md-2 control-label">Nama Barang</label>
   <div class="col-md-10">
      <input class="form-control" value="<?php echo $barang->nama_barang ;?>" name="nama_barang" type="text" placeholder="Nama Barang" required>
   </div>
</div>
<div class="form-group">
   <label class="col-md-2 control-label">Kategori</label>
   <div class="col-md-10">
      <select name="kategori_id" class="form-control" required>
         <option style="display:none">Select</option>
         <?php foreach($kategori as $key) { ?>
         <option <?php if($barang->kategori_id == $key->id_kategori) { echo "selected"; } ?> value="<?php echo $key->id_kategori;?>"><?php echo $key->nama;?></option>
         <?php } ?>
      </select>
   </div>
</div>
<div class="form-group">
   <label class="col-md-2 control-label">Harga Jual</label>
   <div class="col-md-10">
      <input class="form-control" value="<?php echo $barang->jual ;?>" name="jual" type="text" placeholder="Harga Jual" required>
   </div>
</div>
<?php } ?>
