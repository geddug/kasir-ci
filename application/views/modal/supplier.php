<?php if($cek == 0) { ?>
<div class="form-group">
   <label class="col-md-2 control-label">Nama Supplier</label>
   <div class="col-md-10">
      <input class="form-control" name="nama" type="text" placeholder="Nama Supplier" required>
   </div>
</div>
<div class="form-group">
   <label class="col-md-2 control-label">Alamat</label>
   <div class="col-md-10">
      <input class="form-control" name="alamat" type="text" placeholder="Alamat" required>
   </div>
</div>
<div class="form-group">
   <label class="col-md-2 control-label">Telp</label>
   <div class="col-md-10">
      <input class="form-control" name="telp" type="text" placeholder="No Telp" required>
   </div>
</div>
<?php } else { ?>
<input type="hidden" name="id_supplier" value="<?php echo $supplier->id_supplier;?>">
<div class="form-group">
   <label class="col-md-2 control-label">Nama Supplier</label>
   <div class="col-md-10">
      <input class="form-control" value="<?php echo $supplier->nama;?>" name="nama" type="text" placeholder="Nama Supplier" required>
   </div>
</div>
<div class="form-group">
   <label class="col-md-2 control-label">Alamat</label>
   <div class="col-md-10">
      <input class="form-control" value="<?php echo $supplier->alamat;?>" name="alamat" type="text" placeholder="Alamat" required>
   </div>
</div>
<div class="form-group">
   <label class="col-md-2 control-label">Telp</label>
   <div class="col-md-10">
      <input class="form-control" value="<?php echo $supplier->telp;?>" name="telp" type="text" placeholder="No Telp" required>
   </div>
</div>
<?php } ?>
