<?php if($cek == 0) { ?>
<div class="form-group">
   <label class="col-md-2 control-label">Akses</label>
   <div class="col-md-10">
      <input class="form-control" name="akses" type="text" placeholder="Nama Akses" required>
   </div>
</div>
<div class="form-group">
   <label class="col-md-2 control-label">Menu</label>
   <div class="col-md-10">
      <?php foreach ($menu as $key) {?>
      <div class="radio radio-info">
         <input type="radio" name="menu_id" value="<?php echo $key->id_menu;?>" id="<?php echo $key->menu;?>">
         <label for="<?php echo $key->menu;?>"><?php echo $key->menu;?>
         </label>
      </div>
      <?php } ?>
   </div>
</div>
<?php } else { ?>
<input type="hidden" name="id_akses" value="<?php echo $akses->id_akses;?>">
<div class="form-group">
   <label class="col-md-2 control-label">Akses</label>
   <div class="col-md-10">
      <input value="<?php echo $akses->akses;?>" class="form-control" name="akses" type="text" placeholder="Nama Akses" required>
   </div>
</div>
<div class="form-group">
   <label class="col-md-2 control-label">Menu</label>
   <div class="col-md-10">
      <?php $res = $akses->menu_id;
         $res1 = explode(',',$res);
         foreach ($menu as $key) {?>
      <div class="radio eadio-info">
         <input type="radio" <?php foreach ($res1 as $key1 => $value1) {  if ($value1 == $key->id_menu) echo 'checked = "checked"'; }?> name="menu_id" id="<?php echo $key->menu;?>" value="<?php echo $key->id_menu;?>">
         <label for="<?php echo $key->menu;?>">  <?php echo $key->menu;?>
         </label>
      </div>
      <?php } ?>
   </div>
</div>
<?php } ?>
