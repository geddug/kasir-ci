<?php if($cek == 0) { ?>
<?php } else { ?>
<input type="hidden" name="id_toko" value="<?php echo $toko->id_toko;?>">
<div class="form-group">
   <label class="col-md-2 control-label">Nama Toko</label>
   <div class="col-md-10">
      <input class="form-control" value="<?php echo $toko->nama_toko;?>" name="nama_toko" type="text" placeholder="Nama Toko" required>
   </div>
</div>
<div class="form-group">
   <label class="col-md-2 control-label">Alamat</label>
   <div class="col-md-10">
      <input class="form-control" value="<?php echo $toko->alamat;?>" name="alamat" type="text" placeholder="Alamat" required>
   </div>
</div>
<div class="form-group">
   <label class="col-md-2 control-label">Telp</label>
   <div class="col-md-10">
      <input class="form-control" value="<?php echo $toko->telp;?>" name="telp" type="text" placeholder="No Telp" required>
   </div>
</div>
<div class="form-group">
   <label class="col-md-2 control-label">Catatan Di Struk/Nota</label>
   <div class="col-md-10">
      <textarea name="catatan" class="form-control" required><?php echo $toko->catatan;?></textarea>
   </div>
</div>
<?php } ?>
