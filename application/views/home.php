<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
   <!-- Start content -->
   <div class="content">
      <div class="container">
         <div class="row">
            <div class="col-xs-12">
               <div class="page-title-box">
                  <h4 class="page-title">Dashboard</h4>
                  <ol class="breadcrumb p-0 m-0">
                     <li class="active">
                        Dashboard
                     </li>
                  </ol>
                  <div class="clearfix"></div>
               </div>
            </div>
         </div>
         <!-- end row -->
         <div class="row">
            <div class="col-lg-6">
               <div class="panel panel-color panel-info">
                  <div class="panel-heading">
                     <h3 class="panel-title">Stok Barang Yang Akan Habis</h3>
                  </div>
                  <div class="panel-body">
                     <div class="table-responsive">
                        <table class="table table table-hover m-0">
                           <thead>
                              <tr>
                                 <th>Nama Barang</th>
                                 <th>Stok</th>
                              </tr>
                           </thead>
                           <tbody>
                              <?php foreach ($barang as $key) { ?>
                              <tr>
                                 <td><?php echo $key->nama_barang;?></td>
                                 <td><?php echo $key->stok;?></td>
                              </tr>
                              <?php } ?>
                           </tbody>
                        </table>
                     </div>
                     <!-- table-responsive -->
                  </div>
               </div>
            </div>
            <!-- end col -->
            <div class="col-lg-6">
               <div class="panel panel-color panel-info">
                  <div class="panel-heading">
                     <h3 class="panel-title">Penjualan terakhir</h3>
                  </div>
                  <div class="panel-body">
                     <div class="table-responsive">
                        <table class="table table table-hover m-0">
                           <thead>
                              <tr>
                                 <th>ID Struk</th>
                                 <th>Total</th>
                                 <th>Waktu</th>
                              </tr>
                           </thead>
                           <tbody>
                              <?php foreach($struk as $key) { ?>
                              <tr>
                                 <td><?php echo $key->id_struk;?></td>
                                 <td><?php echo number_format($key->total_harga);?></td>
                                 <td><?php echo $key->tgl;?></td>
                              </tr>
                              <?php } ?>
                           </tbody>
                        </table>
                     </div>
                     <!-- table-responsive -->
                  </div>
               </div>
               <!-- end panel -->
            </div>
            <!-- end col -->
         </div>
         <!-- end row -->
      </div>
      <!-- container -->
   </div>
   <!-- content -->
   <footer class="footer text-right">
      <?php echo date("Y");?> © Yaha.
   </footer>
</div>
<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ============================================================== -->