   <!-- Sweet Alert -->
      <link href="<?php echo base_url();?>assets/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css">
	   <!-- Sweet-Alert  -->
      <script src="<?php echo base_url();?>assets/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
<!-- HOME -->
<section>
   <div class="container-alt">
      <div class="row">
         <div class="col-sm-12">
            <div class="wrapper-page">
               <div class="m-t-40 account-pages">
                  <div class="text-center account-logo-box">
                     <h2 class="text-uppercase">
                        <font style="color:#fff">Login</font>
                     </h2>
                     <!--<h4 class="text-uppercase font-bold m-b-0">Sign In</h4>-->
                  </div>
                  <div class="account-content">
                     <form class="form-horizontal" id="formlogin">
                        <div class="form-group ">
                           <div class="col-xs-12">
                              <input class="form-control" type="text" required="" name="username" placeholder="Username">
                           </div>
                        </div>
                        <div class="form-group">
                           <div class="col-xs-12">
                              <input class="form-control" type="password" name="password" required="" placeholder="Password">
                           </div>
                        </div>
                        <div class="form-group ">
                           <div class="col-xs-12">
                              <div class="checkbox checkbox-success">
                                 <input id="checkbox-signup" type="checkbox" checked>
                                 <label for="checkbox-signup">
                                 Remember me
                                 </label>
                              </div>
                           </div>
                        </div>
                        <div class="form-group account-btn text-center m-t-10">
                           <div class="col-xs-12">
                              <button class="btn w-md btn-bordered btn-danger waves-effect waves-light" type="submit">Masuk</button>
                           </div>
                        </div>
                     </form>
                     <br><br><br>
                     <div class="clearfix"></div>
                  </div>
               </div>
               <!-- end card-box-->
            </div>
            <!-- end wrapper -->
         </div>
      </div>
   </div>
</section>
<!-- END HOME -->
<script>
$("#formlogin").on("submit",(function(b){b.preventDefault();$("#modalloading").modal({backdrop:"static",keyboard:false},"show");$.ajax({url:"<?php echo base_url();?>",type:"POST",data:new FormData(this),contentType:false,cache:false,processData:false,success:function(a){if(a>0){$("#isibody").load("<?php echo base_url();?>home")}else{swal("Error","username/password salah","error")}$("#modalloading").modal("hide")},error:function(a,e,f){swal("Error","","error")}});return false}));
</script>
<script>
   var resizefunc = [];
</script>
<!-- jQuery  -->
<script src="<?php echo base_url();?>assets/js/detect.js"></script>
<script src="<?php echo base_url();?>assets/js/fastclick.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.blockUI.js"></script>
<script src="<?php echo base_url();?>assets/js/waves.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.slimscroll.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.scrollTo.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/switchery/switchery.min.js"></script>
<!-- Counter js  -->
<script src="<?php echo base_url();?>assets/plugins/waypoints/jquery.waypoints.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/counterup/jquery.counterup.min.js"></script>
<!-- App js -->
<script src="<?php echo base_url();?>assets/js/jquery.core.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.app.js"></script>
