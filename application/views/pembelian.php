<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
   <!-- Start content -->
   <div class="content">
      <div class="container">
         <div class="row">
            <div class="col-xs-12">
               <div class="page-title-box">
                  <h4 class="page-title">Pembelian </h4>
                  <ol class="breadcrumb p-0 m-0">
                     <li>
                        <a href="#">Beranda</a>
                     </li>
                     <li class="active">
                        Pembelian
                     </li>
                  </ol>
                  <div class="clearfix"></div>
               </div>
            </div>
         </div>
         <!-- end row -->
         <div class="row">
            <div class="col-sm-12">
               <div class="card-box table-responsive">
                  <h4 class="m-t-0 header-title">
                     <b>
                     <button onclick="tambah()" class="btn btn-custom waves-effect waves-light m-b-5">
                     <span class="ion-ios7-plus-empty"></span></button>
                     </b>
                  </h4>
                  <table id="datatable" class="display table table-striped table-bordered">
                     <thead>
                        <tr>
                           <th>Barang</th>
                           <th>Qty</th>
                           <th>Harga Total</th>
                           <th>Supplier</th>
                           <th>User</th>
                           <th>Tanggal</th>
                           <th>Aksi</th>
                        </tr>
                     </thead>
                     <tbody>
                        <?php foreach($pembelian as $key) { ?>
                        <tr>
                           <td><?php echo $key->nama_barang;?></td>
                           <td><?php echo $key->qty;?></td>
                           <td>Rp. <?php echo number_format($key->total);?></td>
                           <td><?php echo $key->nama;?></td>
                           <td><?php echo $key->nama_lengkap;?></td>
                           <td><?php echo date("d F Y H:i:s", strtotime($key->tgl));?></td>
                           <td>
                              <a href="#" onclick="ganti('<?php echo $key->id_pembelian;?>')" class="table-action-btn h3">
                              <i class="mdi mdi-pencil-box-outline text-success"></i>
                              </a>
                           </td>
                        </tr>
                        <?php } ?>
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
      <!-- container -->
   </div>
   <!-- content -->
   <footer class="footer text-right">
      <?php echo date("Y");?> © Yaha.
   </footer>
</div>
<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ============================================================== -->
<!-- sample modal content -->
<div id="myModal" class="modal fade" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="myModalLabel">Form</h4>
         </div>
         <form class="form-horizontal" role="form" id="form">
            <div class="modal-body" id="modalbody">
            </div>
            <div class="modal-footer" id="loading">
               <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
               <button type="submit" class="btn btn-primary waves-effect waves-light">Simpan</button>
            </div>
         </form>
      </div>
      <!-- /.modal-content -->
   </div>
   <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<script type="text/javascript">
var table;var simpan;$(document).ready(function(){var a=$("#datatable").DataTable()});function tambah(){simpan="tambah";$("#form")[0].reset();$("#myModal").modal("show");$("#modalbody").load("<?php echo base_url();?>modalpembelian/",function(a){$("#modalbody").html(a);$(".select2").select2()})}function ganti(a){simpan="update";$("#form")[0].reset();$("#myModal").modal("show");$("#modalbody").load("<?php echo base_url();?>editpembelian/"+a,function(b){$("#modalbody").html(b);$(".select2").select2()})}$("#form").on("submit",(function(b){b.preventDefault();var a;if(simpan=="tambah"){a="<?php echo base_url();?>addpembelian"}else{a="<?php echo base_url();?>updatepembelian"}$.ajax({url:a,type:"POST",data:new FormData(this),contentType:false,cache:false,processData:false,success:function(c){$("#myModal").modal("hide");swal({title:"Sukses",text:"Berhasil",type:"success"},function(){$.get("<?php echo base_url();?>pembelian",function(e,d){$("#pindah").html(e)})})},error:function(c,e,d){swal("Error","","error")}});return false}));
</script>
